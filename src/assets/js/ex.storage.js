/**
 * Module local storage:
 *
 * ```javascript
 * ex.storage.set('storage', 123)
 * ex.storage.get('storage')
 * ex.storage.remove('storage')
 * ex.storage.getAll()
 * ex.storage.clear()
 *
 * // if object
 * ex.storage.set('storage', {yyy: 123})
 * ex.storage.get('storage', key)
 * ex.storage.remove('storage', key)
 * ```
 */
ex.storage = (function ($) {
    var isObject = function (value) {
        return ((typeof value === "object") && (value !== null));
    }
    var getKeys = function () {
        var keys = [];
        $.each(Object.keys(localStorage), function (i, key) {
            if (key.search(pub.STORAGE_KEY) !== -1) {
                keys.push(key.split('_').pop());
            }

        });
        return keys;
    }

    var pub = {
        STORAGE_KEY: 'ex_',

        get: function (storage, key) {
            if (key) {
                var item = this.getItem(storage);
                return item[key];
            } else {
                return this.getItem(storage);
            }
        },

        getAll: function () {
            var values = [];

            $.each(getKeys(), function(i, key) {
                values[key] = pub.getItem(key);
            });
            return values;
        },

        set: function (storage, value) {
            var result;
            if (isObject(value)) {
                var item = this.getItem(storage);
                if (isObject(item)) {
                    result = $.extend({}, item, value);
                } else {
                    result = value;
                }
            } else {
                result = value;
            }
            this.setItem(storage, result);
        },

        setItem: function (key, value) {
            localStorage.setItem(this.STORAGE_KEY + key, JSON.stringify(value));
        },

        getItem: function (key) {
            return JSON.parse(localStorage.getItem(this.STORAGE_KEY + key));
        },

        remove: function (storage, key) {
            if (key) {
                var item = this.getItem(storage);
                delete item[key];
                this.setItem(storage, item);
            } else {
                localStorage.removeItem(this.STORAGE_KEY + storage);
            }
        },

        clear: function (storage) {
            $.each(getKeys(), function(i, key) {
                pub.remove(key);
            });
        }
    }

    if (typeof(Storage) === "undefined") {
        return;
    }

    return pub;
})(jQuery);

// Tests
// ex.storage.set('storage', 123)
// console.log(ex.storage.get('storage'));
// ex.storage.remove('storage')
// ex.storage.clear()
//
// ex.storage.set('storage', {yyy: 7777})
// ex.storage.set('storage', {ccc: 8888})
// console.log(ex.storage.get('storage', 'yyy'));
// ex.storage.remove('storage', 'yyy')
