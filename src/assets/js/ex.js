ex = (function ($) {
    var pub = {
        isActive: true,
        init: function () {
            UIkit.modal.labels = {
                ok: 'Да',
                cancel: 'Отмена'
            };
            initFontAwesome();
            initExActions();
            checkSwitcherFormErrors();
            initIcon();

            $('[ex-autosize]').each(function() {
                autosize(this)
            }).on('autosize:resized', function(){
                autosize.update(this)
            })

            // ajaxLoadModal();

            $('.fr-wrapper').on('load', function (e, editor) {
                $('[data-f-id="pbf"]').remove();
                console.log(this);
            });

            $(document).ajaxComplete(function(event, xhr, settings) {
                initIcon();
            });
            $(document).ajaxSuccess(function(event, xhr, settings, data) {
                notification(data);
            });


            // $(window).bind('keydown', function(event) {
            //     if (event.ctrlKey || event.metaKey) {
            //         switch (String.fromCharCode(event.which).toLowerCase()) {
            //         case 's':
            //             event.preventDefault();
            //             alert('ctrl-s');
            //             break;
            //         case 'f':
            //             event.preventDefault();
            //             alert('ctrl-f');
            //             break;
            //         case 'g':
            //             event.preventDefault();
            //             alert('ctrl-g');
            //             break;
            //         }
            //     }
            // });
        },
        /**
        * Displays a confirmation dialog.
        * The default implementation simply displays a js confirmation dialog.
        * You may override this by setting `ex.confirm`.
        * @param message the confirmation message.
        * @param ok a callback to be called when the user confirms the message
        * @param cancel a callback to be called when the user cancels the confirmation
        */
        confirm: function (message, ok, cancel) {
            UIkit.modal.confirm(message).then(function () {
                ok();
            }, function () {
                !ok;
            });
        },
        copyToClipboard: function (data) {
            var $temp = $('<input>');
            $('body').append($temp);
            $temp.val(data).select();
            var exec = document.execCommand('copy');
            $temp.remove();
            return exec;
        },
        reloadPjax: function($e) {
            if ($.support.pjax) {
                if (typeof $e === 'string') {
                    container = $e;
                } else {
                    container = $e.closest('[data-pjax-container]').attr('id')
                        ? ('#' + $e.closest('[data-pjax-container]').attr('id'))
                        : '';
                }

                if (container) {
                    $.pjax.reload({container});
                }
            }
        },
        /**
        * jQuery AJAX submit form
        * Example:
        *
        * ```javascript
        * ex.ajaxSubmitForm('form#contacts', { pjaxReload: true, closeModal: true });
        * ```
        * Event:
        *
        * ```javascript
        * $(document).on('afterSubmit', 'form#contacts', function(e, data) {
        *     console.log(data);
        * });
        * ```
        */
        ajaxSubmitForm: function($form, options) {
            var defaultOptions = {
                pjaxReload: false,
                closeModal: false
            };
            var options = $.extend({}, defaultOptions, options);

            if (typeof $form === 'string') {
                $form = $($form);
            }

            $form.on('beforeSubmit', function(e) {
                if ($form.find('.has-error').length) {
                    return false;
                }

                $.ajax({
    	            type: $form.attr('method'),
    	            url: $form.attr('action'),
    	            data: $form.serialize(),
                    success: function(data, textStatus, jqXHR) {
                        $form.trigger('afterSubmit', [data, textStatus, jqXHR]);

                        if (data.status = 'success') {
                            $form.yiiActiveForm('resetForm');
                        } else if (data.validation) {
                            $form.yiiActiveForm('updateMessages', data.validation, true);
                        }

                        if (options.pjaxReload) {
                            pub.reloadPjax(options.pjaxContainer || $form);
                        }

                        if (options.closeModal) {
                            var modal = $form.closest(options.modalContainer || '.uk-modal');
                            if (modal.length) {
                                UIkit.modal(modal).hide();
                            }
                        }
                    }
                });

                return false;
            });
        },
        /**
         * Handles the action triggered by user.
         * For example, you may use the following code to generate
         * such a link:
         *
         * ```php
         * use yii\helpers\Html;
         *
         * echo Html::a('Delete', ['site/delete'], [
         *     'ex' => [
         *         'ajax' => true,
         *         'confirm' => Yii::t('system', 'Are you sure you want to delete the selected items?'),
         *         'selected' => '#fooGrid', // selected grid rows (data-params=[ids])
         *         'pjax-container' => '#fooPjax' // container pjax reload
         *         'params' => [
         *             'name1' => 'value1',
         *             'name2' => 'value2',
         *         ],
         *         'success' => "console.log(data)"
         *     ],
         * ]);
         *
         * echo Html::a('Add', ['site/add'], [
         *     'ex' => [
         *         'modal' => true,
         *         'params' => [
         *             'name1' => 'value1',
         *             'name2' => 'value2',
         *         ],
         *         'success' => "console.log(data)"
         *     ],
         * ]);
         * ```
         *
         * @param $e the jQuery representation of the element
         * @param event Related event
         */
        execAction: function ($e, event) {
            var modal = $e.attr('ex-modal');
            var ajax = $e.attr('ex-ajax');
            var pjaxContainer = $e.attr('ex-pjax-container');
            var success = $e.attr('ex-success');

            var settings = {
                url: $e.attr('href') || $e.attr('ex-url'),
                data: $.extend({}, $e.attr('ex-params'), $e.data('params')),
                type: $e.attr('ex-method')
            };

            var isValidAction = settings.url && settings.url !== '#';

            if (!isValidAction) {
                return true;
            }

            if (modal !== undefined) {
                settings.type = settings.type || 'get';
                $.ajax(settings).done(function(data) {
                    // $.ajaxPrefilter(function(options, original_Options, jqXHR) {
                    //     options.async = true;
                    // });
                    if (!$(data).find('.uk-modal-body').addBack('.uk-modal-body').length) {
                        data = $('<div class="uk-modal-body">').html(data);
                    }
                    var modalOptions = modal ? JSON.parse(modal) : {};
                    ex.dialog(data, modalOptions).show();
                });
            } else if (ajax !== undefined) {
                settings.type = settings.type || 'post';
                settings.dataType = $e.attr('ex-datatype') || 'json',
                $.ajax(settings).done(function(data, textStatus, jqXHR) {
                    $e.trigger('ajaxSuccess', [data, textStatus, jqXHR]);
                    pub.reloadPjax(pjaxContainer || $e);

                    if (success) {
                        eval(success);
                    }
                });
            }
        },
        dialog: function(data, options) {
            var options = $.extend({}, { center: false }, options)
            var dialog = UIkit.modal('<div class="uk-modal"> <div class="uk-modal-dialog"> </div> </div>', options)
    		var $modal = $(dialog.$el)
    		var $dialog = $modal.find('.uk-modal-dialog')
            var $data = $(data)

            switch (options.width) {
                case 'expand':
                    $dialog.addClass('uk-width-1-1')
                    break;
                case 'container':
                    $modal.addClass('uk-modal-container');
                    break;
                case 'full':
                    $modal.addClass('uk-modal-full');
                    $dialog
                        .attr('uk-height-viewport', '')
                        .append('<button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>');
                    break;
                default:
                    $dialog.width(options.width);
            }

            if (options.height == 'expand') {
                // $dialog.addClass('uk-height-1-1')
                // $data.find('.uk-modal-body').height($dialog.height())
            }
            if (options.overflow) {
                $data.find('.uk-modal-body').addBack('.uk-modal-body').attr('uk-overflow-auto', '')
            }
            if (options.center) {
    			$modal.addClass('uk-flex-top');
                $dialog.addClass('uk-margin-auto-vertical');
    		}

    		UIkit.util.on(dialog.$el, 'show', function() {
    			$(this).find('.uk-modal-dialog').html($data);
    		});

            UIkit.util.on(dialog.$el, 'hidden', function (ref) {
                var target = ref.target;
                var currentTarget = ref.currentTarget;

                if (target === currentTarget) {
                    Promise.resolve(function () { return dialog.$destroy(true); });
                    $modal.remove();
                }
            });

    		return dialog;
        }
    };

    function initExActions() {
        var handler = function (event) {
            var $this = $(this),
                ajax = $this.attr('ex-ajax'),
                modal = $this.attr('ex-modal'),
                confirm = $this.attr('ex-confirm');

            if (ajax === undefined && modal === undefined && confirm === undefined) {
                return true;
            }

            if (confirm !== undefined) {
                $.proxy(pub.confirm, this)(confirm, function () {
                    pub.execAction($this, event);
                });
            } else {
                pub.execAction($this, event);
            }
            event.stopImmediatePropagation();
            return false;
        };

        // handle ex-confirm and other ex actions for clickable and changeable elements
        $(document).on('click.yii', yii.clickableSelector, handler)
            .on('change.yii', yii.changeableSelector, handler);
    }

    function checkSwitcherFormErrors() {
        var switcher = $('.uk-switcher');
        var form = switcher.closest('form');

        form.on('afterValidate ajaxComplete', function () {
            form.find(switcher).each(function () {
                var el = $(this);
                var list = el.children('li');
                list.each(function(index, element) {
                    var error = $(this).find('.has-error');

                    if (error.length) {
                        tab = $('[uk-tab*="' + el.attr('id') + '"]>li:eq(' + index + ')');
                        tab.trigger('click');

                        return false;
                    }
                });
            });
        });
    }

    /*
    * Define a pseudo-element in CSS (content: "\f146";)
    */
    function initFontAwesome() {
        if (typeof FontAwesomeConfig === "undefined") return;
        window.FontAwesomeConfig.searchPseudoElements = true;
    }

    function initIcon() {
        var attrIcon = 'ex-icon';
        $('[' + attrIcon + ']').each(function () {
            var $icon = $('<i>'),
                value = $(this).attr(attrIcon);
            $(this).html($icon.addClass(value)).removeAttr(attrIcon);
        });
    }

    function notification(data) {
        if (data !== undefined) {
            var status = (data.status == 'success' || data.result) ? 'success' : 'danger';

            if (data.notification !== undefined) {
                UIkit.notification(data.notification, status)
            } else if (data.alert !== undefined) {
                UIkit.modal.alert(data.alert)
            }
        }
    }

    function parseJson(data) {
        console.log(data);
        return data !== undefined ? $.parseJSON(data) : {};
    }

    return pub;
})(jQuery);

jQuery(document).ready(function () {
    yii.initModule(ex);
    yii.confirm = ex.confirm;
});
