<?php

namespace exoo\kit;

use yii\web\AssetBundle;

/**
 * ExooKit asset bundle.
 */
class ExookitAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@exoo/kit/assets';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/ex.css',
    ];
    /**
     * @inheritdoc
     */
    public $js = [
        'js/ex.js',
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
    	'exoo\uikit\UikitAsset',
        'exoo\fontawesome\FontAwesomeAsset',
    ];
}
