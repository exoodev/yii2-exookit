<?php

namespace exoo\exookit\docs;

/**
 * ExooKit.
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $layout = 'main';
}
