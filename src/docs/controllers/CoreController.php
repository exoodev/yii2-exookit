<?php

namespace exoo\exookit\docs\controllers;

use Yii;
use yii\web\Controller;

/**
 * Description and examples of using ExooKit core components.
 */
class CoreController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionColor()
    {
        return $this->render('color');
    }
}
