<?php

use yii\helpers\Html;
use exoo\uikit\Nav;

$menuItems = [
    [
        'label' => 'Core',
        'url' => ['/kit/docs/core'],
        'active' => Yii::$app->controller->id == 'docs/core'
    ],
    ['label' => 'Components', 'url' => ['/kit/docs/components']],
    ['label' => 'Application', 'url' => ['/']],
];

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <?php $this->beginBody() ?>

    <div class="tm-navbar uk-navbar-container">
        <div class="uk-container uk-container-expand">
            <nav uk-navbar>
                <div class="uk-navbar-left">
                    <div class="uk-navbar-item uk-logo">Documentation for EXOOkit</div>
                </div>
                <div class="uk-navbar-right">
                    <?= Nav::widget([
                        'options' => ['class' => 'uk-navbar-nav'],
                        'items' => $menuItems,
                    ]) ?>
                </div>
            </nav>
        </div>
    </div>

    <div class="uk-section">
        <div class="uk-container uk-container-expand">

            <?php if (isset($this->blocks['sidebar-a'])) : ?>
                <div class="uk-grid-divider" uk-grid>
                    <div class="uk-width-1-6@m">
                        <div class="tm-sidebar-a">
                            <?= $this->blocks['sidebar-a'] ?>
                        </div>
                    </div>
                    <div class="uk-width-expand@m">
                        <?= $content ?>
                    </div>
                </div>
            <?php else: ?>
                <?= $content ?>
            <?php endif; ?>

        </div>
    </div>

    <?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
