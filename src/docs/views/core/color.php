<?php

use yii\helpers\Html;
use exoo\widgets\ColorAsset;

$this->title = 'Colors';
ColorAsset::register($this);

$this->beginBlock('sidebar-a');
echo $this->render('_sidebar');
$this->endBlock();

$css = <<<CSS
.color-link {
    background: rgba(0, 0, 0, 0.38);
    padding: 2px 7px;
    color: #d4d4d4;
    display: none;
    position: absolute;
    right: 15px;
    top: 14px;
}
.color-link>a:hover {
    color: #fff !important;
}
CSS;
$this->registerCss($css);
?>

<h1><?= Html::encode($this->title) ?></h1>
<div id="colorsGrid" class="uk-grid-small uk-child-width-1-5@m" uk-grid></div>
<h3>Css classes</h3>
<button type="button" class="uk-button uk-button-primary js-generate">Generate</button>
<div id="colorsCssClasses" class="uk-hidden">
    <ul class="uk-subnav uk-subnav-pill" uk-switcher>
        <li><a href="#">Text</a></li>
        <li><a href="#">Background</a></li>
        <li><a href="#">Border</a></li>
    </ul>

    <ul class="uk-switcher uk-margin">
        <li><pre><code id="colorsTextClasses"></code></pre></li>
        <li><pre><code id="colorsBackgroundClasses"></code></pre></li>
        <li><pre><code id="colorsBorderClasses"></code></pre></li>
    </ul>
</div>


<?php
$js = <<<JS
var prefix = {
    text: 'ex-text-',
    border: 'ex-border-',
    background: 'ex-background-',
};

renderTiles();

$(document).on('click', '.js-generate', function() {
    $(this).remove();
    renderCssClasses();
    $('#colorsCssClasses').removeClass('uk-hidden');
})

function renderTiles() {
    if (!colors) return;
    var grid = $('#colorsGrid');

    var template = '<div class="uk-tile uk-padding-small uk-text-small">';
    $.each(colors, function(name, styles) {
        var column = $('<div>');
        styles[0] = styles[500];

        $.each(styles, function(code, color) {
            var tile = $(template);
            var key = (code == 0 ? name : name + '-' + code);
            tile.css({
                'background-color': color,
                'color': (code >= 500 && code <= 900 || code == 0) ? '#fff' : '#292929',
                'font-weight': 'bold',
                'text-transform': 'capitalize'
            });
            tile.html([
                '<span>' + (code == 0 ? name : code) + '</span>',
                '<span class="uk-float-right color-link">',
                    '<a class="uk-link-reset uk-margin-small-right" data-key="'+prefix.text+key+'">',
                        '<i class="fas fa-font" uk-tooltip="Text"></i>',
                    '</a>',
                    '<a class="uk-link-reset uk-margin-small-right" data-key="'+prefix.background+key+'">',
                        '<i class="fas fa-lg fa-square" uk-tooltip="Background"></i>',
                    '</a>',
                    '<a class="uk-link-reset" data-key="'+prefix.border+key+'">',
                        '<i class="far fa-lg fa-square" uk-tooltip="Border"></i>',
                    '</a>',
                '</span>',
            ].join(''));
            column.append(tile);
        });

        grid.append(column);
    });

    $(document).on('click', '#colorsGrid a', function() {
        if (ex.copyToClipboard($(this).data('key'))) {
            UIkit.notification('<i class="fas fa-lg fa-check-circle uk-margin-small-right"></i> Copied to clipboard!', 'success');
        }
    });

    $('#colorsGrid .uk-tile').hover(
        function() {
            $(this).find('.color-link').fadeIn('fast');
        },
        function() {
            $(this).find('.color-link').fadeOut('fast');
        }
    );
}

function renderCssClasses() {
    if (!colors) return;

    $.each(colors, function(name, styles) {
        styles[0] = styles[500];
        $.each(styles, function(code, color) {
            var key = (code == 0 ? name : name + '-' + code);
            $('#colorsTextClasses').append([
                '.'+prefix.text+key+' {',
                '\\tcolor: '+color+';',
                '}\\n',
            ].join('\\n'));

            $('#colorsBackgroundClasses').append([
                '.'+prefix.background+key+' {',
                '\\tbackground-color: '+color+';',
                '}\\n',
            ].join('\\n'));

            $('#colorsBorderClasses').append([
                '.'+prefix.border+key+' {',
                '\\tborder-color: '+color+';',
                '}\\n',
            ].join('\\n'));
        });
    });

    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
}

JS;
$this->registerJs($js);
