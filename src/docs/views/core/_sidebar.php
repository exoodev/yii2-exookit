<?php
use yii\helpers\Html;
use exoo\widgets\Nav;

$items = [
    [
        'header' => 'STYLE'
    ],
    [
        'url' => ['docs/core/color'],
        'label' => 'Color'
    ],
];
?>

<?= Nav::widget([
    'items' => $items,
    'options' => [
        'class' => 'uk-nav uk-nav-side'
    ],
]) ?>
<a class="uk-button uk-button-primary uk-button-large uk-width-1-1 uk-margin-large-top" href="https://getuikit.com/v2/docs/core.html" target="_blank">More</a>
